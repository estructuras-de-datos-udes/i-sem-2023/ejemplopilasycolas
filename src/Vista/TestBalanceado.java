/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Lab06pcdocente
 */
public class TestBalanceado {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Digite una cadena");
        String cadena = teclado.nextLine();
        Stack<Character> pila = new Stack();
        boolean sw=true;
        for (int i = 0; i < cadena.length(); i++) {
            char letra = cadena.charAt(i);
            if (letra == '(') {
                pila.push(letra);
            } else {
                if (letra == ')') {
                    if (!pila.empty()) {
                        pila.pop();
                    } else {
                            sw=false;
                            break;
                    }
                }

            }

        }
        if(pila.empty() && sw)
            System.out.println("La expresion es balanceada");
        else
            System.out.println("No esta balanceada");
    }
}
