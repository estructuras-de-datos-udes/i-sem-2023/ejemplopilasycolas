/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author madar
 */
public class TestCola {

    public static void main(String[] args) {
        Queue<Integer> cola = new LinkedList();
        Scanner teclado = new Scanner(System.in);
        System.out.println("Cuantos datos desea en la Cola:");
        int n = teclado.nextInt();
        if (n <= 0) {
            System.out.println("Error no puedo crear la cola");
        } else {
            //Comportamiento FIFO
            Random numero = new Random();
            while (n > 0) {
                int dato = numero.nextInt(100);
                cola.offer(dato);
                n--;
            }
            //Imprimir la cola:
            while(!cola.isEmpty())
                System.out.print(cola.poll()+"\t");
        }

    }

}
