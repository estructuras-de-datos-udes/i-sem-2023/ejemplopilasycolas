/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author madar
 */
public class SistemaAcademico {

    private Queue<Estudiante> estudiantes = new LinkedList();

    public SistemaAcademico() {
    }

    public boolean insertarEstudiante(Estudiante nuevo) {
        return this.estudiantes.offer(nuevo); //v si lo pudo insertar
    }

    public String getListado() {
        String msg = "";
        //La cola de respaldo
        Queue<Estudiante> estudiantes2 = new LinkedList();
        while (!this.estudiantes.isEmpty()) {
            Estudiante x = this.estudiantes.poll();
            msg += x.toString() + "\n";
            estudiantes2.add(x);
        }
        this.estudiantes = estudiantes2;

        return msg;
    }

    public String getConsulta(int codigo) {
        Queue<Estudiante> estudiantes2 = new LinkedList();
        String mensaje = "Estudiante con código:" + codigo + ", no existe";
        while (!estudiantes.isEmpty()) {
            Estudiante x = estudiantes.poll();
            if (x.getCodigo() == codigo) {
                mensaje = x.toString();
            }
            //Esto lo hago para no perder los datos:
            estudiantes2.add(x);
        }
        this.estudiantes = estudiantes2;
        return mensaje;
    }

    
    public String getConsulta(byte d, byte a, byte m)
    {
        return null;
    }
}
