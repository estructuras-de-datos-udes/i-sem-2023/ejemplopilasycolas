/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class Estudiante implements Comparable {

    private String nombre;
    private int codigo;
    private byte d, a, m;

    public Estudiante() {
    }

    public Estudiante(String nombre, int codigo, byte d, byte a, byte m) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.d = d;
        this.a = a;
        this.m = m;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public byte getD() {
        return d;
    }

    public void setD(byte d) {
        this.d = d;
    }

    public byte getA() {
        return a;
    }

    public void setA(byte a) {
        this.a = a;
    }

    public byte getM() {
        return m;
    }

    public void setM(byte m) {
        this.m = m;
    }

    private int hash() {
        return (int) this.a * 1000 + this.m * 100 + this.d * 10;
    }

    @Override
    public int compareTo(Object o) {
        Estudiante dos = (Estudiante) o;
        return this.hash() - dos.hash();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + this.codigo;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estudiante other = (Estudiante) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Estudiante{" + "nombre=" + nombre + ", codigo=" + codigo + ", d=" + d + ", a=" + a + ", m=" + m + '}';
    }
    
    
}
